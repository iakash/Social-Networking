const knex = require('./connection');

module.exports = {
  // To get all records of category table
  getAllCategories() {
    return knex('categories');
  },
  // To get value by offset
  getCategoriesByOffset(limit, offset) {
    return knex('categories').limit(limit).offset(offset);
  },
  // To get 1 records of category table
  getCategory(category_id_pk){
    return knex('categories').where('category_id_pk', category_id_pk).first();
  },
  // To create records in category table
  createCategory(category) {
    return knex('categories').insert(category, '*');
  },
  // To update 1 record of category table
  updateCategory(id, category){
    return knex('categories').where('category_id_pk', id).update(category, '*');
  },
  // To delete record of category table
  deleteCategory(id) {
    return knex('categories').where('category_id_pk', id).del();
  },
  patchCategory(id, category) {
    return knex('categories').where('category_id_pk', id).update({
      category_name: category.category_name
    }, '*');
  }
}
