const knex = require('./connection');
const strftime = require('strftime');

module.exports = {
  getAllGroups() {
    return knex('groups');
  },
  getGroupsByOffset(limit, offset) {
    return knex('groups').limit(limit).offset(offset);
  },
  getGroup(id){
    return knex('groups').where('group_id_pk', id).first();
  },
  createGroup(group) {
    const currentTimestamp = strftime('%F %T', new Date())
    group.group_created_date = currentTimestamp;
    group.group_status = 'active';
    return knex('groups').insert(group, '*');
  },
  updateGroup(id, group){
    const currentTimestamp = strftime('%F %T', new Date());
    group.group_update_date = currentTimestamp;
    return knex('groups').where('group_id_pk', id).update(group, '*');
  },
  deleteGroup(id) {
    const currentTimestamp = strftime('%F %T', new Date());
    return knex('groups').where('group_id_pk', id).update({
      group_deleted_date: currentTimestamp,
      group_status: 'deleted'
    }, '*');
  },
  patchGroup(id, group) {
    return knex('groups').where('group_id_pk', id).update({
      group_name: group.group_name
    }, '*');
  }
}
