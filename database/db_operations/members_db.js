const knex = require('./connection');

module.exports = {
  getAllMembers() {
    return knex('members');
  },
  getMembersByOffset(limit, offset) {
    return knex('members').limit(limit).offset(offset);
  },
  getMember(id){
    return knex('members').where('member_id_pk', id).first();
  },
  createMember(member) {
    member.member_status = 'active';
    return knex('members').insert(member, '*');
  },
  updateMember(id, member){
    return knex('members').where('member_id_pk', id).update(member, '*');
  },
  deleteMember(id) {
    return knex('members').where('member_id_pk', id).update({
      member_status: 'deleted'
    }, '*');
  },
  patchMember(id, member) {
    return knex('members').where('member_id_pk', id).update({
      member_name: member.category_name
    }, '*');
  }
}
