const knex = require('./connection');

module.exports = {
  // To create user by email id
  createLocalUser(email, password) {
    return knex('users').insert({
      email_id: email,
      password: password,
      provider: 'local',
    }, '*');
  },
  // find user by email id
  findByEmail(email){
    return knex('users').where('email_id', email).select('*').first();
  },
  // find user by member_id
  findUserById(id){
    return knex('users').where('member_id', id).select('*').first();
  },
  // to create member in members table while sunging up user
  createMemberViaSignup(member_name, email){
    return this.findByEmail(email)
      .then((user) => {
        return knex('members').insert({
          member_name: member_name,
          member_id_pk: user.member_id,
          member_email_id: user.email_id,
        }, '*')
          .catch((error) => {
            throw error
          });
      });
  },
};
