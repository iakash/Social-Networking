Prerequisite:
===

- Node should be installed to run this application.Below is the link to download node.   
     https://www.npmjs.com/get-npm      
- PostgreSQL should be install to run this application. Below is the link to download PostgreSQL.      
     https://www.postgresql.org/download/    
- Run below command to install required packages.
```      
     npm install   
```
- Run below command to install required bower components.
```
     npm run bower install  
```
- Set user, database, host, and password in knexfile.js in development object.
- Run below command first to test :
```
    npm test
```
---------

Usage
===
- Run below command to start the server
```
   npm start
```
- Enter below link in browser    
  http://localhost:4000/index 