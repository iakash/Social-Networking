const express = require('express');
const path = require('path');
// const expressValidator = require('express-validator');
// const favicon = require('serve-favicon');
// var logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const knex = require('./database/db_operations/connection.js');
const passport = require('passport');
const flash = require('connect-flash');

// Routes

const members = require('./routes/members');
const categories = require('./routes/categories');
const groups = require('./routes/groups.js');
const events = require('./routes/events.js');
const users = require('./routes/users');

// const users = require('./routes/users');
//
const app = express();

// Array of paths that contains pug files
const viewPaths = [path.join(__dirname, 'views'), path.join(__dirname, 'views/Categories'), path.join(__dirname, 'views/Members'), path.join(__dirname, 'views/Groups'), path.join(__dirname, 'views/Events')];

app.use(cookieParser());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use(passport.initialize());
app.use(passport.session());

app.use(require('express-session')({
  secret: 'kjhifhakdhgadfafgagf',
  resave: true,
  saveUninitialized: false,
  cookie: { maxAge: 60000000 },
}));

app.use(flash());

app.use(function (req, res, next) {
  res.locals.delete_msg = req.flash('delete_msg');
  res.locals.update_msg = req.flash('update_msg');
  res.locals.error_msg = req.flash('error_msg');
  res.locals.user_msg = req.flash('user_msg');
  next();
})

// view engine setup
app.set('view engine', 'pug');
app.set('views', viewPaths);
app.use(express.static(path.join(__dirname, 'public')));

// All below queries are for calculate total rows in the table to identify how many pages is required for pagination
// and to set count of row to variable to use it later

knex('categories').count('category_id_pk').first()
  .then((count) => {
    app.set('categoriesCount', count.count);
  })
  .catch((error) => { throw error; });

knex('members').count('member_id_pk').first()
  .then((count) => {
    app.set('membersCount', count.count);
  })
  .catch((error) => { throw error; });

knex('events').count('event_id_pk').first()
  .then((count) => {
    app.set('eventsCount', count.count);
  })
  .catch((error) => { throw error; });

knex('groups').count('group_id_pk').first()
  .then((count) => {
    app.set('groupsCount', count.count);
  })
  .catch((error) => { throw error; });

app.get('/index', function(req, res){
  res.render('index');
});


app.use('/user', users);
app.use('/categories', categories);
app.use('/members', members);
app.use('/groups', groups);
app.use('/events', events);


app.use((req, res) => {
  res.header('Access-Control-Allow-Headers', '*');
});

// app.use(expressValidator({
//   errorFormatter: function(param, msg, value) {
//     const namespace = param.split('.')
//     const root = namespace.shift()
//     const formParam = root;

//     while (namespace.length) {
//       formParam += '[' + namespace.shift() + ']';
//     }

//     return {
//       param: formParam,
//       msg: msg,
//       value: value
//     };
//   },
// }));
// app.use(expressValidator());


// uncomment after placing your favicon in /public
// app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
// app.use(logger('dev'));
// app.use(bodyParser.json());
// app.use(bodyParser.urlencoded({ extended: false }));
// app.use(cookieParser());
// app.use(express.static(path.join(__dirname, 'public')));

// app.use('/', index);
// app.use('/users', users);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
