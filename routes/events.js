const express = require('express');
const router = express.Router();
const eventsQuery = require('../database/db_operations/events_db.js');
const { validEvent } = require('./validations');

// To display events table
router.get('/', function(req, res) {
  const page = Number(req.query.page) || 1;
  const pageStart = Number(req.query.pageStart) || 1;
  let lastPage = pageStart + 10;

  // if remaining data to display only require less then 10 pages then it sets last page value accordingly
  let pages = Number(req.app.get('eventsCount')) / 10;
  if (lastPage > Math.ceil(pages)) {
    lastPage = Math.ceil(pages) - (lastPage - 11);
  }

  eventsQuery.getEventsByOffset(10, (page - 1) * 10).then((events) => {
    res.render('eventlist', {
      events: events,
      pageStart: pageStart,
      lastPage: lastPage,
    });
  });
});

// To search event by id
router.post('/search', function(req, res) {
  const event_id_pk = req.body.event_id_pk;
  if (!Number.isNaN(event_id_pk)) {
    eventsQuery.getEvent(event_id_pk)
      .then(event => {
        if (event) {
          res.status(200).render('search_event', {
            event: event,
          });
        } else {
          req.flash('error_msg', 'Event does not exist');
          res.redirect('/events');
        }
      })
      .catch((error) => {
        res.render('error', {
          error: error,
        });
      });
  } else {
    req.flash('error_msg', 'Invalid Id');
    res.redirect('/events');
  }
});


// To display add_event.pug
router.get('/add', function(req, res) {
  res.render('add_event');
});

// To add event in to events table
router.post('/add', function(req, res) {
  const event_id_pk = req.body.event_id_pk;
  if (!Number.isNaN(event_id_pk)) {
    eventsQuery.getEvent(event_id_pk)
      .then(event => {
        if (event) {
          res.status(200).render('add_event', {
            message: 'Event allready exist',
          });
          // next(new Error('event Allready exist'));
        } else if (validEvent(req.body)) {
          eventsQuery.createEvent(req.body)
            .then(() => {
              res.status(200).render('add_event', {
                message: 'Event added successfully',
              });
            })
            .catch((error) => {
              res.render('error', {
                error: error,
              });
            });
        } else {
          res.status(200).render('add_event', {
            message: 'Invalid event',
          });
        // next(new Error('Invalid event'));
        }
      })
      .catch((error) => {
        res.render('error', {
          error: error,
        });
      });
  } else {
    res.status(200).render('add_event', {
      message: 'Invalid Id',
    });
  }
});

// To delete event by id
router.get('/delete/:id', function(req, res) {
  const event_id_pk = req.params.id;
  eventsQuery.getEvent(event_id_pk).then(event => {
    if (event) {
      eventsQuery.deleteEvent(event_id_pk).then(() => {
        req.flash('delete_msg', 'Event deleted successfully');
        res.redirect('/events');
      }).catch((error) => {
        res.render('error', {
          error: error,
        });
      });
    }
    // next(new Error('Event does not exist'))
  }).catch((error) => {
    res.render('error', {
      error: error,
    });
  });
});


// To display update form of event
router.get('/update/:event_id', function(req, res) {
  const event_id_pk = req.params.event_id;
  eventsQuery.getEvent(event_id_pk).then((event) => {
    if (event) {
      res.render('update_event', {
        event: event,
      });
    }
  }).catch(() => {
    res.render('eventlist', {
      message: 'Error while searching event',
    });
  });
});

// To update event in events table
router.post('/update/:event_id', function(req, res) {
  const event_id_pk = req.body.event_id_pk;
  if (!Number.isNaN(event_id_pk) && validEvent(req.body) && event_id_pk === req.params.event_id) {
    eventsQuery.getEvent(event_id_pk)
      .then(event => {
        if (event) {
          eventsQuery.updateEvent(event_id_pk, req.body).then(() => {
            req.flash('update_msg', 'Event updated successfully');
            res.redirect('/events');
          })
            .catch((error) => {
              res.render('error', {
                error: error,
              });
            });
        }
      })
      .catch((error) => {
        res.render('error', {
          error: error,
        });
      });
  } else {
    eventsQuery.getEvent(event_id_pk)
      .then(event => {
        if (event) {
          res.render('update_event', {
            message: 'Please enter all fields',
            event: event,
          });
        }
      })
      .catch((error) => {
        res.render('error', {
          error: error,
        });
      });
  }
});

module.exports = router;
