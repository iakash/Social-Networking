const express = require('express');
const router = express.Router();
const membersQuery = require('../database/db_operations/members_db.js');
const { validMember } = require('./validations');

// To display members table
router.get('/', function(req, res) {
  const page = Number(req.query.page) || 1;
  const pageStart = Number(req.query.pageStart) || 1;
  let lastPage = pageStart + 10;

  // if remaining data to display only require less then 10 pages then it sets last page value accordingly
  let pages = Number(req.app.get('membersCount')) / 10;
  if (lastPage > Math.ceil(pages)) {
    lastPage = Math.ceil(pages) - (lastPage - 11);
  }

  membersQuery.getMembersByOffset(10, (page - 1) * 10).then((members) => {
    if (members) {
      res.render('memberlist', {
        members: members,
        pageStart: pageStart,
        lastPage: lastPage,
      });
    }
  });
});

// To search member by id
router.post('/search', function(req, res) {
  const member_id_pk = req.body.member_id_pk;
  if (!Number.isNaN(member_id_pk)) {
    membersQuery.getMember(member_id_pk)
      .then(member => {
        if (member) {
          res.status(200).render('search_member', {
            member: member,
          });
        } else {
          req.flash('error_msg', 'Member does not exist');
          res.redirect('/members');
        }
      })
      .catch((error) => {
        res.render('error', {
          error: error,
        });
      });
  } else {
    req.flash('error_msg', 'Invalid Id');
    res.redirect('/members');
  }
});

// To display add_member.pug
router.get('/add', function(req, res) {
  res.render('add_member');
});

// To search member by id
router.post('/add', function(req, res) {
  const member_id_pk = req.body.member_id_pk;
  if (!Number.isNaN(member_id_pk)) {
    membersQuery.getMember(member_id_pk)
      .then(member => {
        if (member) {
          res.status(200).render('add_member', {
            message: 'Member allready exist',
          });
          // next(new Error('member Allready exist'));
        } else if (validMember(req.body)) {
          membersQuery.createMember(req.body).then(() => {
            res.status(200).render('add_member', {
              message: 'Member added successfully',
            });
          })
            .catch((error) => {
              res.render('error', {
                error: error,
              });
            });
        } else {
          res.status(200).render('add_member', {
            message: 'Invalid member',
          });
        // next(new Error('Invalid member'));
        }
      })
      .catch((error) => {
        res.render('error', {
          error: error,
        });
      });
  } else {
    res.status(200).render('add_member', {
      message: 'Invalid Id',
    });
  }
});

// To delete member by id
router.get('/delete/:id', function(req, res) {
  const member_id_pk = req.params.id;
  membersQuery.getMember(member_id_pk).then(member => {
    if (member) {
      membersQuery.deleteMember(member_id_pk).then(() => {
        req.flash('delete_msg', 'Member deleted successfully');
        res.redirect('/members');
      }).catch((error) => {
        res.render('error', {
          error: error,
        });
      });
    }
    // next(new Error('Category does not exist'))
  }).catch((error) => {
    res.render('error', {
      error: error,
    });
  });
});


// To display update form of member
router.get('/update/:member_id', function(req, res) {
  const member_id_pk = req.params.member_id;
  membersQuery.getMember(member_id_pk).then((member) => {
    if (member) {
      res.render('update_member', {
        member: member,
      });
    }
  }).catch(() => {
    res.render('memberlist', {
      message: 'Error while searching member',
    });
  });
});

// To update member in members table
router.post('/update/:member_id', function(req, res) {
  const member_id_pk = req.body.member_id_pk;
  if (!Number.isNaN(member_id_pk) && validMember(req.body) && member_id_pk === req.params.member_id) {
    membersQuery.getMember(member_id_pk)
      .then(member => {
        if (member) {
          membersQuery.updateMember(member_id_pk, req.body).then(() => {
            req.flash('update_msg', 'Member updated successfully');
            res.redirect('/members');
          })
            .catch((error) => {
              res.render('error', {
                error: error,
              });
            });
        }
      })
      .catch((error) => {
        res.render('error', {
          error: error,
        });
      });
  } else {
    membersQuery.getMember(member_id_pk)
      .then(member => {
        if (member) {
          res.render('update_member', {
            message: 'Please enter all fields',
            member: member,
          });
        }
      })
      .catch((error) => {
        res.render('error', {
          error: error,
        });
      });
  }
});

module.exports = router;

// router.patch('/:id', isValidId, function(req, res, next) {
//   // res.render('index', { title: 'member called' });
//   membersQuery.getMember(req.params.id).then(member => {
//     if (category) {
//       membersQuery.patch(req.params.id, req.body).then((updatedCategory) => {
//         res.status(200).json(updatedCategory);
//       });
//     } else {
//       next(new Error('Category does not exist'));
//     }
//   });
// });

// router.put('/:id', isValidId, function(req, res, next) {
//   // res.render('index', { title: 'member called' });
//   membersQuery.getMember(req.params.id).then(category => {
//     if (category) {
//       if (validMember(req.body)) {
//         membersQuery.update(req.params.id, req.body).then(categories => {
//           res.status(200).json(categories[0]);
//         });
//       } else {
//         next(new Error('Invalid Category'));
//       }
//     } else {
//       next(new Error('Category does not exist'));
//     }
//   });
// });

