const express = require('express');
const router = express.Router();
const groupsQuery = require('../database/db_operations/groups_db.js');
const { validGroup } = require('./validations');

// To display groups table
router.get('/', function(req, res) {
  const page = Number(req.query.page) || 1;
  const pageStart = Number(req.query.pageStart) || 1;
  let lastPage = pageStart + 10;

  // if remaining data to display only require less then 10 pages then it sets last page value accordingly
  let pages = Number(req.app.get('groupsCount')) / 10;
  if (lastPage > Math.ceil(pages)) {
    lastPage = Math.ceil(pages) - (lastPage - 11);
  }

  groupsQuery.getGroupsByOffset(10, (page - 1) * 10).then((groups) => {
    res.render('grouplist', {
      groups: groups,
      pageStart: pageStart,
      lastPage: lastPage,
    });
  });
});

// To search group by id
router.post('/search', function(req, res) {
  const group_id_pk = req.body.group_id_pk;
  if (!Number.isNaN(group_id_pk)) {
    groupsQuery.getGroup(group_id_pk)
      .then(group => {
        if (group) {
          res.status(200).render('search_group', {
            group: group,
          });
        } else {
          req.flash('error_msg', 'Group does not exist');
          res.redirect('/groups');
        }
      })
      .catch((error) => {
        res.render('error', {
          error: error,
        });
      });
  } else {
    req.flash('error_msg', 'Invalid Id');
    res.redirect('/groups');
  }
});

// To display add_group.pug
router.get('/add', function(req, res) {
  res.render('add_group');
});

// To add group in groups table
router.post('/add', function(req, res) {
  const group_id_pk = req.body.group_id_pk;
  if (!Number.isNaN(group_id_pk)) {
    groupsQuery.getGroup(group_id_pk)
      .then(group => {
        if (group) {
          res.status(200).render('add_group', {
            message: 'Group allready exist',
          });
          // next(new Error('group Allready exist'));
        } else if (validGroup(req.body)) {
          groupsQuery.createGroup(req.body).then(() => {
            res.status(200).render('add_group', {
              message: 'Group added successfully',
            });
          })
            .catch((error) => {
              res.render('error', {
                error: error,
              });
            });
        } else {
          res.status(200).render('add_group', {
            message: 'Invalid group',
          });
        // next(new Error('Invalid group'));
        }
      })
      .catch((error) => {
        res.render('error', {
          error: error,
        });
      });
  } else {
    res.status(200).render('add_group', {
      message: 'Invalid Id',
    });
  }
});

// To delete group by id
router.get('/delete/:id', function(req, res) {
  const group_id_pk = req.params.id;
  groupsQuery.getGroup(group_id_pk).then(group => {
    if (group) {
      groupsQuery.deleteGroup(group_id_pk).then(() => {
        req.flash('delete_msg', 'Group deleted successfully');
        res.redirect('/groups');
      }).catch((error) => {
        res.render('error', {
          error: error,
        });
      });
    }
    // next(new Error('Category does not exist'))
  }).catch((error) => {
    res.render('error', {
      error: error,
    });
  });
});


// To display update form of group
router.get('/update/:group_id', function(req, res) {
  const group_id_pk = req.params.group_id;
  groupsQuery.getGroup(group_id_pk).then((group) => {
    if (group) {
      res.render('update_group', {
        group: group,
      });
    }
  }).catch(() => {
    res.render('grouplist', {
      message: 'Error while searching group',
    });
  });
});

// To update group in groups table
router.post('/update/:group_id', function(req, res) {
  const group_id_pk = req.body.group_id_pk;
  if (!Number.isNaN(group_id_pk) && validGroup(req.body) && group_id_pk === req.params.group_id) {
    groupsQuery.getGroup(group_id_pk)
      .then(group => {
        if (group) {
          groupsQuery.updateGroup(group_id_pk, req.body).then(() => {
            req.flash('update_msg', 'Group updated successfully');
            res.redirect('/groups');
          })
            .catch((error) => {
              res.render('error', {
                error: error,
              });
            });
        }
      })
      .catch((error) => {
        res.render('error', {
          error: error,
        });
      });
  } else {
    groupsQuery.getGroup(group_id_pk)
      .then(group => {
        if (group) {
          res.render('update_group', {
            message: 'Please enter all fields',
            group: group,
          });
        }
      })
      .catch((error) => {
        res.render('error', {
          error: error,
        });
      });
  }
});

module.exports = router;
