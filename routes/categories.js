const express = require('express');
const router = express.Router();
const categoriesQuery = require('../database/db_operations/categories_db.js');
const { validCategory } = require('./validations');
// const app = express();

// To display categories table
router.get('/', function(req, res) {
  const page = Number(req.query.page) || 1;
  const pageStart = Number(req.query.pageStart) || 1;
  let lastPage = pageStart + 10;

  // if remaining data to display only require less then 10 pages then it sets last page value accordingly
  let pages = Number(req.app.get('categoriesCount')) / 10;
  if (lastPage > Math.ceil(pages)) {
    lastPage = Math.ceil(pages) - (lastPage - 11);
  }

  categoriesQuery.getCategoriesByOffset(10, (page - 1) * 10).then((categories) => {
    res.render('categorylist', {
      categories: categories,
      pageStart: pageStart,
      lastPage: lastPage,
    });
  });
});

// To search category by Id
router.post('/search', function(req, res) {
  const category_id_pk = req.body.category_id_pk
  if (!Number.isNaN(category_id_pk)) {
    categoriesQuery.getCategory(category_id_pk)
      .then(category => {
        if (category) {
          res.status(200).render('search_category', {
            category: category,
          });
        } else {
          req.flash('error_msg', 'Category does not exist');
          res.redirect('/categories');
        }
      })
      .catch((error) => {
        res.render('error', {
          error: error,
        });
      });
  } else {
    req.flash('error_msg', 'Invalid Id');
    res.redirect('/categories');
  }
});

// To display add_category.pug
router.get('/add', function(req, res) {
  res.render('add_category');
});

// To add category into categories table
router.post('/add', function(req, res) {
  const category_id_pk = req.body.category_id_pk
  if (!Number.isNaN(category_id_pk)) {
    categoriesQuery.getCategory(category_id_pk)
      .then(category => {
        if (category) {
          res.status(200).render('add_category', {
            message: 'Category allready exist',
          });
          // next(new Error('Category Allready exist'));
        } else if (validCategory(req.body)) {
          categoriesQuery.createCategory(req.body)
            .then(() => {
              res.status(200).render('add_category', {
                message: 'Category added successfully',
              });
            })
            .catch((error) => {
              res.render('error', {
                error: error,
              });
            });
        } else {
          res.status(200).render('add_category', {
            message: 'Invalid Category',
          });
          // next(new Error('Invalid Category'));
        }
      })
      .catch((error) => {
        res.render('error', {
          error: error,
        });
      });
  } else {
    res.status(200).render('add_category', {
      message: 'Invalid Id',
    });
  }
});

// To delete category by id
router.get('/delete/:id', function(req, res) {
  const category_id_pk = req.params.id;
  categoriesQuery.getCategory(category_id_pk).then(category => {
    if (category) {
      categoriesQuery.deleteCategory(category_id_pk).then(() => {
        req.flash('delete_msg', 'Category deleted successfully');
        res.redirect('/categories');
      }).catch((error) => {
        res.render('error', {
          error: error,
        });
      });
    }
    // next(new Error('Category does not exist'))
  }).catch((error) => {
    res.render('error', {
      error: error,
    });
  });
});


// To display update form of category
router.get('/update/:category_id', function(req, res) {
  const category_id_pk = req.params.category_id;
  categoriesQuery.getCategory(category_id_pk).then((category) => {
    if (category) {
      res.render('update_category', {
        category: category,
      });
    }
  }).catch(() => {
    res.render('categorylist', {
      message: 'Error while searching category',
    });
  });
});

// To update category in categories table
router.post('/update/:category_id', function(req, res) {
  const category_id_pk = req.body.category_id_pk;
  if (!Number.isNaN(category_id_pk) && validCategory(req.body) && category_id_pk === req.params.category_id) {
    categoriesQuery.getCategory(category_id_pk)
      .then(category => {
        if (category) {
          categoriesQuery.updateCategory(category_id_pk, req.body).then(() => {
            req.flash('update_msg', 'Category updated successfully');
            res.redirect('/categories');
          })
            .catch((error) => {
              res.render('error', {
                error: error,
              });
            });
        }
      })
      .catch((error) => {
        res.render('error', {
          error: error,
        });
      });
  } else {
    categoriesQuery.getCategory(category_id_pk)
      .then(category => {
        if (category) {
          res.render('update_category', {
            message: 'Please enter all fields',
            category: category,
          });
        }
      })
      .catch((error) => {
        res.render('error', {
          error: error,
        });
      });
  }
});

module.exports = router;
