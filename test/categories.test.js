const request = require('supertest');
const app = require('../app');

describe('GET /categories', () => {
  it('Should display categories table with records of Categories', (done) => {
    request(app)
      .get('/categories')
      .expect('content-type', 'text/html; charset=utf-8')
      .expect(200)
      .expect(/category_id_pk/, done);
  });
});

describe('GET /categories/add', () => {
  it('Should display Form to add category in Categories table', (done) => {
    request(app)
      .get('/categories/add')
      .expect('content-type', 'text/html; charset=utf-8')
      .expect(200, done);
  });
});

describe('POST /categories/add', () => {
  it('Should add category to Categories table', (done) => {
    request(app)
      .post('/categories/add')
      .send({
        category_id_pk: '46',
        category_name: 'Travelling',
      })
      .expect('content-type', 'text/html; charset=utf-8')
      .expect(200)
      .expect(/(Category allready exist|Category added successfully)/g, done);
  });
});

describe('POST /categories/search', () => {
  it('Should search category from Categories table', (done) => {
    request(app)
      .post('/categories/search')
      .send({
        category_id_pk: '46',
      })
      .expect('content-type', 'text/html; charset=utf-8')
      .expect(200)
      .expect(/46/, done);
  });
});

describe('GET /categories/update/46', () => {
  it('Should display form filled with category details to update category in Categories table', (done) => {
    request(app)
      .get('/categories/update/46')
      .expect('content-type', 'text/html; charset=utf-8')
      .expect(200)
      .expect(/46/, done);
  });
});

describe('POST /categories/update/46', () => {
  it('Should update the category and redirect to /categories path', (done) => {
    request(app)
      .post('/categories/update/46')
      .send({
        category_id_pk: '46',
        category_name: 'Tour & Travelling',
      })
      .expect(302)
      .expect('location', /\/categories/, done);
  });
});

describe('GET /categories/delete/46', () => {
  it('Should delete the category and redirect to /categories path ', (done) => {
    request(app)
      .get('/categories/delete/46')
      .expect(302)
      .expect('location', /\/categories/, done);
  });
});

