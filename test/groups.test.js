const request = require('supertest');
const app = require('../app');

describe('GET /groups', () => {
  it('Should display groups table with records of groups', (done) => {
    request(app)
      .get('/groups')
      .expect('content-type', 'text/html; charset=utf-8')
      .expect(200)
      .expect(/group_id_pk/, done);
  });
});

describe('GET /groups/add', () => {
  it('Should display Form to add group in groups table', (done) => {
    request(app)
      .get('/groups/add')
      .expect('content-type', 'text/html; charset=utf-8')
      .expect(200, done);
  });
});

describe('POST /groups/add', () => {
  it('Should add group to groups table', (done) => {
    request(app)
      .post('/groups/add')
      .send({
        group_id_pk: '6388',
        group_name: 'Alternative Health NYC',
        group_members: '1440',
        group_type: 'public',
        category_id_fk: '14',
        group_description: 'Meet fellow LGBT Real Estate Buyers and Investors in the San Francisco Bay Area. Come to a local Real Estate Buying & Investing Meetup to discuss properties on the market and get real estate investing education. You can find more information on property investing on www.SFBAI.com',
      })
      .expect('content-type', 'text/html; charset=utf-8')
      .expect(200)
      .expect(/(Group allready exist|Group added successfully)/g, done);
  });
});

describe('POST /groups/search', () => {
  it('Should search group from groups table', (done) => {
    request(app)
      .post('/groups/search')
      .send({
        group_id_pk: '6388',
      })
      .expect('content-type', 'text/html; charset=utf-8')
      .expect(200)
      .expect(/6388/, done);
  });
});

describe('GET /groups/update/6388', () => {
  it('Should display form filled with group details to update group in groups table', (done) => {
    request(app)
      .get('/groups/update/6388')
      .expect('content-type', 'text/html; charset=utf-8')
      .expect(200)
      .expect(/6388/, done);
  });
});

describe('POST /groups/update/6388', () => {
  it('Should update the group and redirect to /groups path', (done) => {
    request(app)
      .post('/groups/update/6388')
      .send({
        group_id_pk: '6388',
        group_name: 'Alternative Health NYC',
        group_members: '1500',
        group_type: 'public',
        category_id_fk: '14',
        group_description: 'Meet fellow LGBT Real Estate Buyers and Investors in the San Francisco Bay Area. Come to a local Real Estate Buying & Investing Meetup to discuss properties on the market and get real estate investing education. You can find more information on property investing on www.SFBAI.com',
      })
      .expect(302)
      .expect('location', /\/groups/, done);
  });
});

describe('GET /groups/delete/6388', () => {
  it('Should delete the group and redirect to /groups path ', (done) => {
    request(app)
      .get('/groups/delete/6388')
      .expect(302)
      .expect('location', /\/groups/, done);
  });
});
