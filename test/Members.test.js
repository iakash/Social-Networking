const request = require('supertest');
const app = require('../app');

describe('GET /members', () => {
  it('Should display Members table with records of Members', (done) => {
    request(app)
      .get('/members')
      .expect('content-type', 'text/html; charset=utf-8')
      .expect(200)
      .expect(/active/, done);
  });
});

describe('GET /members/add', () => {
  it('Should display Form to add member in Members table', (done) => {
    request(app)
      .get('/members/add')
      .expect('content-type', 'text/html; charset=utf-8')
      .expect(200, done);
  });
});

describe('POST /members/add', () => {
  it('Should add member to Members table', (done) => {
    request(app)
      .post('/members/add')
      .send({
        member_id_pk: '1025',
        member_name: 'Yohan Lee',
        member_email_id: 'YohanLee@gmail.com',
        member_phone_no: '1826702489',
        member_address: '5st.,Central Park,New York',
        member_state: 'New York',
        member_country: 'US',
        member_zip_code: '123423',
        member_status: 'active',
      })
      .expect('content-type', 'text/html; charset=utf-8')
      .expect(200)
      .expect(/(Member added successfully|Member allready exist)/g, done);
  });
});

describe('POST /members/search', () => {
  it('Should search member from Members table', (done) => {
    request(app)
      .post('/members/search')
      .send({
        member_id_pk: '1025',
      })
      .expect('content-type', 'text/html; charset=utf-8')
      .expect(200)
      .expect(/1025/, done);
  });
});

describe('GET /members/update/1025', () => {
  it('Should display form filled with member details to update member in Members table', (done) => {
    request(app)
      .get('/members/update/1025')
      .expect('content-type', 'text/html; charset=utf-8')
      .expect(200)
      .expect(/1025/, done);
  });
});

describe('POST /members/update/1025', () => {
  it('Should update the member and redirect to /members path', (done) => {
    request(app)
      .post('/members/update/1025')
      .send({
        member_id_pk: '1025',
        member_name: 'Yohan Lee',
        member_email_id: 'YohanLee@yahoo.com',
        member_phone_no: '4567702489',
        member_address: '5st.,Central Park,New York',
        member_state: 'New York',
        member_country: 'US',
        member_zip_code: '123423',
      })
      .expect(302)
      .expect('location', /\/members/, done);
  });
});

describe('GET /members/delete/1025', () => {
  it('Should delete the member and redirect to /members path ', (done) => {
    request(app)
      .get('/members/delete/1025')
      .expect(302)
      .expect('location', /\/members/, done);
  });
});
