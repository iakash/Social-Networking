const request = require('supertest');
const app = require('../app');

describe('GET /events', () => {
  it('Should display events table with records of Events', (done) => {
    request(app)
      .get('/events')
      .expect('content-type', 'text/html; charset=utf-8')
      .expect(200)
      .expect(/event_id_pk/, done);
  });
});

describe('GET /events/add', () => {
  it('Should display Form to add event in Events table', (done) => {
    request(app)
      .get('/events/add')
      .expect('content-type', 'text/html; charset=utf-8')
      .expect(200, done);
  });
});

describe('POST /events/add', () => {
  it('Should add event to Events table', (done) => {
    request(app)
      .post('/events/add')
      .send({
        event_id_pk: '1538682',
        event_name: 'Solve the murder case while eating',
        event_description: 'Solve the murder case while eating a 3 course meal at 3 different restaurants in North Beach.We will be joining my friends meetup group and a group of travelers for appetizer salad, gourmet pizza, drinks, and dessert - and a murder mystery that needs to be solved! Since the menu and restaurants change sometimes, here is the main RSVP link, full menu, and current updated info:http://www.meetup.com/SanFranciscoHostel/events/222306617/</a> EVENT INFO:<a href=http://www.sfhostelparty.com/>www.sfhostelparty.com We eat a different course at each place - we stay at each restaurant for about 40 minutes before heading off to the next one. RSVP is *required* for planning purposes. You *must* meet us at the first place between 7:30PM and 7:45PM if you are planning to attend - please do not try and join us mid-way into the tour.',
        event_date: '2018-04-30 02:30:00',
        event_duration: '20700',
        event_created_date: '2018-02-03 21:24:29',
        group_id_fk: '5817262',
        member_id: '182670248',
      })
      .expect('content-type', 'text/html; charset=utf-8')
      .expect(200)
      .expect(/(Event added successfully|Event allready exist)/g, done);
  });
});

describe('POST /events/search', () => {
  it('Should search event from Events table', (done) => {
    request(app)
      .post('/events/search')
      .send({
        event_id_pk: '1538682',
      })
      .expect('content-type', 'text/html; charset=utf-8')
      .expect(200)
      .expect(/1538682/, done);
  });
});

describe('GET /events/update/1538682', () => {
  it('Should display form filled with event details to update event in Events table', (done) => {
    request(app)
      .get('/events/update/1538682')
      .expect('content-type', 'text/html; charset=utf-8')
      .expect(200)
      .expect(/1538682/, done);
  });
});

describe('POST /events/update/1538682', () => {
  it('Should update the event and redirect to /events path', (done) => {
    request(app)
      .post('/events/update/1538682')
      .send({
        event_id_pk: '1538682',
        event_name: 'Solve the murder case',
        event_description: 'Solve the murder case while eating a 3 course meal at 3 different restaurants in North Beach.We will be joining my friends meetup group and a group of travelers for appetizer salad, gourmet pizza, drinks, and dessert - and a murder mystery that needs to be solved! Since the menu and restaurants change sometimes, here is the main RSVP link, full menu, and current updated info:http://www.meetup.com/SanFranciscoHostel/events/222306617/</a> EVENT INFO:<a href=http://www.sfhostelparty.com/>www.sfhostelparty.com We eat a different course at each place - we stay at each restaurant for about 40 minutes before heading off to the next one. RSVP is *required* for planning purposes. You *must* meet us at the first place between 7:30PM and 7:45PM if you are planning to attend - please do not try and join us mid-way into the tour.',
        event_date: '2018-04-30 02:30:00',
        event_duration: '20700',
        event_created_date: '2018-02-03 21:24:29',
        group_id_fk: '5817262',
        member_id: '182670248',
      })
      .expect(302)
      .expect('location', /\/events/, done);
  });
});

describe('GET /events/delete/1538682', () => {
  it('Should delete the event and redirect to /events path ', (done) => {
    request(app)
      .get('/events/delete/1538682')
      .expect(302)
      .expect('location', /\/events/, done);
  });
});
