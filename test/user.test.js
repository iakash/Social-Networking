const request = require('supertest');
const app = require('../app');

// const knex = require('../database/db_operations/connection.js');
const User = require('../database/db_operations/users-db.js');

describe('GET /user/signup', () => {
  it('Should display signup page', (done) => {
    request(app)
      .get('/user/signup')
      .expect('content-type', 'text/html; charset=utf-8')
      .expect(200)
      .expect(/Sign up/, done);
  });
});

describe('POST /user/signup', () => {
  it('Should redirect to login page', (done) => {
    const user = {
      member_name: 'Yohan Lee',
      email: 'test@gmail.com',
      password: '1234qwer',
    };

    User.findByEmail(user.email).then((SignedUpUser) => {
      if (SignedUpUser) {
        request(app)
          .post('/user/signup')
          .send(user)
          .expect(200)
          .expect(/duplicate key value violates unique constraint/, done);
      } else {
        request(app)
          .post('/user/signup')
          .send(user)
          .expect(302)
          .expect('location', /\/user\/login/, done);
      }
    });
  });
});

describe('GET /user/login', () => {
  it('Should display login page', (done) => {
    request(app)
      .get('/user/login')
      .expect('content-type', 'text/html; charset=utf-8')
      .expect(200)
      .expect(/Login/, done);
  });
});

describe('POST /user/login', () => {
  it('Should redirect to index page', (done) => {
    request(app)
      .post('/user/login')
      .send({
        email: 'test@gmail.com',
        password: '1234qwer',
      })
      .expect(302)
      .expect('location', /\/index/, done);
  });
});
